# README #

Simple dll injector, shows a few techniques:


* Playing .xm music (using fmod) from a resource
* flickerfree scrolling text using gdi (yes, it's no opengl quicktime)
* loading bitmaps from resources
* using BitBlt to draw transparent images (colorkey)
* using NtQuerySystemInformation to scan processes for the running one (no more psapi / toolhelp!)

### To use it ###

* change data/loader.bmp to any bmp of your liking (should automatically resize the program)
* change music.xm to any .xm of your liking (hint: keygenmusic)
* edit main.cpp at the top, to use the correct text, game and window title
* The dllname must be the same as the exename, change in remoteload.cpp if u dont like that
* fix the 3 anti copypasta errors, every .cpp one :)

##ONLY IN RELEASE THE SOUND WILL PLAY##

credits:
msdn (most functions), some guy at codeguru for the idea of the transparent bitmaps, random sites at google for the structs from remoteload.h
