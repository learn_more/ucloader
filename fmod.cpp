#define _WIN32_WINNT 0x0501
#define _WIN32_WINDOWS 0x0410
#define _WIN32_IE 0x0600

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

#ifndef _DEBUG
#define USE_FMOD
#endif


#ifdef USE_FMOD
#include "data/minifmod.h"
#pragma comment( lib, "data/minifmod.lib" )
#pragma comment( lib, "winmm.lib" )
#include "resource.h"


extern "C" {
	extern HWAVEOUT FSOUND_WaveOutHandle;	//l_m: minifmod saves the waveout device in a handle named this, since it's a lib we can use it :)
}
DWORD oldVolume;
DWORD curVolume;

//most of this is copied from fmod example, some small modifications to make it smaller
//and to make it only usable for this :)
//on debug fmod is disabled, it was causing the compiler to whine about all sorta crap


typedef struct 
{
	int length;
	int pos;
	void *data;
} MEMFILE;


MEMFILE memfile = {0};
FMUSIC_MODULE* mod;

unsigned int memopen( char *name )
{
	HRSRC		rec;
	HGLOBAL		handle;		

	rec = FindResourceA( NULL, name, "XM" );		//dont need explaining :)
	handle = LoadResource( NULL, rec );
	memfile.data = LockResource( handle );
	memfile.length = SizeofResource( NULL, rec );
	memfile.pos = 0;

	return (unsigned int)&memfile;
}

void memclose(unsigned int /*handle*/)
{
}

int memread(void *buffer, int size, unsigned int handle)
{
	MEMFILE *memfile = (MEMFILE *)handle;

	if (memfile->pos + size >= memfile->length)
		size = memfile->length - memfile->pos;

	memcpy(buffer, (char *)memfile->data+memfile->pos, size);
	memfile->pos += size;
	
	return size;
}

void memseek(unsigned int handle, int pos, signed char mode)
{
	MEMFILE *memfile = (MEMFILE *)handle;

	if (mode == SEEK_SET) 
		memfile->pos = pos;
	else if (mode == SEEK_CUR) 
		memfile->pos += pos;
	else if (mode == SEEK_END)
		memfile->pos = memfile->length + pos;

	if (memfile->pos > memfile->length)
		memfile->pos = memfile->length;
}

int memtell(unsigned int handle)
{
	MEMFILE *memfile = (MEMFILE *)handle;

	return memfile->pos;
}

#endif	//#ifdef USE_FMOD

//easier interface from main.cpp
void initFMod()
{
#ifdef USE_FMOD
	FSOUND_File_SetCallbacks( memopen, memclose, memread, memseek, memtell );

	mod = FMUSIC_LoadSong( MAKEINTRESOURCEA(IDR_XM1), NULL);
	waveOutGetVolume( FSOUND_WaveOutHandle, &oldVolume );
	curVolume = (DWORD)((float)LOWORD(oldVolume) / 655.35f);
	FMUSIC_PlaySong( mod );
#endif //#ifdef USE_FMOD
}


void setVolumeFMod( DWORD num )
{
#ifdef USE_FMOD
	num = (WORD)((float)num * 655.35f);
	num |= num << 16;
	waveOutSetVolume( FSOUND_WaveOutHandle, num );
	waveOutGetVolume( FSOUND_WaveOutHandle, &curVolume );
	curVolume = (DWORD)((float)LOWORD(curVolume) / 655.35f);
#endif //#ifdef USE_FMOD
}

DWORD getVolumeFMod()
{
#ifdef USE_FMOD
	return curVolume;
#else
	return 0;
#endif //#ifdef USE_FMOD
}


void playFMod()
{
#ifdef USE_FMOD
	FMUSIC_PlaySong( mod );
#endif //#ifdef USE_FMOD
}

void stopFMod()
{
#ifdef USE_FMOD
	FMUSIC_StopSong( mod );
#endif //#ifdef USE_FMOD
}

void endFMod()
{
#ifdef USE_FMOD
	waveOutSetVolume( FSOUND_WaveOutHandle, oldVolume );
	FMUSIC_FreeSong( mod );
#endif //#ifdef USE_FMOD
}
