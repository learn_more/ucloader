#define _WIN32_WINNT 0x0501
#define _WIN32_WINDOWS 0x0410
#define _WIN32_IE 0x0600

#include <windows.h>
#include <string.h>
#include "remoteload.h"


HANDLE hHeap;
PVOID pBuffer;
ULONG cbBuffer = 0x2000;
tNtQuerySystemInformation NtQuerySystemInformation;


bool inject( PSYSTEM_PROCESSES proc )
{
	char sDll[512];
	Sleep( 1000 );		//make sure game is initialized a bit
	
	//get the exename, change .exe to .dll
	DWORD dwLen = GetModuleFileNameA( NULL, sDll, 512 );
	char* cptr = (char*)(sDll + dwLen - 3);
	*cptr++ = 'd'; *cptr++ = 'l'; *cptr++ = 'l';

	//the normal blabla injecting createremotethread
	HANDLE hProc = OpenProcess( PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION | PROCESS_VM_WRITE, true, proc->ProcessId );
	LPVOID dwAddr = VirtualAllocEx( hProc, NULL, 512, MEM_COMMIT, PAGE_READWRITE );

	if( dwAddr )
	{
		SIZE_T dwWritten;
		WriteProcessMemory( hProc, dwAddr, sDll, strlen(sDll)+1, &dwWritten );
		LPTHREAD_START_ROUTINE dwLoadLib = (LPTHREAD_START_ROUTINE)GetProcAddress( GetModuleHandleA("kernel32"), "LoadLibraryA" );
		HANDLE dwThread = CreateRemoteThread( hProc, NULL, 0, dwLoadLib, dwAddr, 0, 0 );
		if( dwThread )
		{
			WaitForSingleObject( dwThread, INFINITE );
			CloseHandle( dwThread );
		} else
		{
			return false;
		}
		VirtualFreeEx( hProc, dwAddr, NULL, MEM_RELEASE );
		CloseHandle( hProc );
		return true;
	}
	return false;

}

extern wchar_t lpwProcessName[512];		//no extra header needed

//psapi and other high level stuff were giving me alotta page faults
//and i needed this routine for an app which was gonna run in the background,
//so i wrote this which generates alot less page faults :)

ULONG update()
{
	NTSTATUS Status;
	do
	{
		if( !pBuffer )
			pBuffer = HeapAlloc(hHeap, 0, cbBuffer);
		if (pBuffer == NULL)
			return NULL;
		Status = NtQuerySystemInformation( SystemProcessesAndThreadsInformation, pBuffer, cbBuffer, NULL );

		if (Status == STATUS_INFO_LENGTH_MISMATCH)
		{
			HeapFree( hHeap, 0, pBuffer );
			pBuffer = 0;
			cbBuffer += 0x2000;
		}
		else if (!NT_SUCCESS(Status))
		{
			HeapFree( hHeap, 0, pBuffer );
			pBuffer = 0;
			return NULL;
		}
	} while (Status == STATUS_INFO_LENGTH_MISMATCH);

	PSYSTEM_PROCESSES pProcesses = (PSYSTEM_PROCESSES)pBuffer;

	for (;;)
	{
		wchar_t * pszProcessName = pProcesses->ProcessName.Buffer;
		if( pszProcessName && pProcesses->ProcessId > 10 )
		{
			if( !wcscmp( pszProcessName, lpwProcessName ) ) {
				inject( pProcesses );
				return 1;
			}
		}
		
		if (pProcesses->NextEntryDelta == 0)
			break;

		pProcesses = (PSYSTEM_PROCESSES)( ((LPBYTE)pProcesses) + pProcesses->NextEntryDelta);
	}
	return NULL;
}



//infinite wait for process

DWORD __stdcall waitFunc(LPVOID)
{
	*(void**)&NtQuerySystemInformation = (void*)GetProcAddress( GetModuleHandleW(L"ntdll"), "ZwQuerySystemInformation" );
	hHeap = GetProcessHeap();
	ULONG findProc = 0;
	while( !findProc ) {
		findProc = update();
		Sleep( 100 );
	}
	if( pBuffer )
		HeapFree( hHeap, 0, pBuffer );

	ExitProcess( NULL );	//done injecting, time to end :)
}

