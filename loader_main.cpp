#define _WIN32_WINNT 0x0501
#define _WIN32_WINDOWS 0x0410
#define _WIN32_IE 0x0600

#include <windows.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include "resource.h"


char scrollTxt[] =				"   ::   Simple UC Loader by learn_more   ::   Credits would be appreciated";
wchar_t lpwProcessName[512] =	L"game.exe";	//guess...
wchar_t lpwTitle[512] =			L"UC Loader";		//loader window title (in taskbar)

//DLL is expected to be same name as the exe, and in same directory!!!!
//see remoteload.cpp to change it



SIZE scrollSize[sizeof(scrollTxt)];
RECT scrollDLG;
int scrollLength;
int curPos;
int nScroll = 0;

HBRUSH hBRblack = NULL;
HDC hBackDC = NULL;
HANDLE hBackh;
BITMAP hBackbmp;
HPEN hBlackPen = NULL;
HPEN hRedPen = NULL;

HANDLE hSndOn, hSndOnMask = NULL;
HANDLE hSndOff, hSndOffMask = NULL;



//__forceinline is teh leet
void __forceinline initBrushes()
{
	//use msdn for params
	HMODULE hMe = GetModuleHandleA(NULL);
	hBackh = LoadImageA( hMe, MAKEINTRESOURCEA(IDB_BACK), IMAGE_BITMAP, NULL, NULL, LR_CREATEDIBSECTION|LR_DEFAULTSIZE );
	hSndOn = LoadImageA( hMe, MAKEINTRESOURCEA(IDB_SNDON), IMAGE_BITMAP, NULL, NULL, LR_CREATEDIBSECTION|LR_DEFAULTSIZE );
	hSndOff = LoadImageA( hMe, MAKEINTRESOURCEA(IDB_SNDOFF), IMAGE_BITMAP, NULL, NULL, LR_CREATEDIBSECTION|LR_DEFAULTSIZE );

	hBRblack = CreateSolidBrush( RGB(0,0,0) );			//for filling the dialog
	GetObject( hBackh, sizeof(BITMAP), &hBackbmp );		//get bitmap info (for dlg size)
	hBlackPen = CreatePen( PS_SOLID, 2, 0 );			//for drawing close x
	hRedPen = CreatePen( PS_SOLID, 2, RGB(255,0,0) );	//l_m: volume
}

void __forceinline destroyBrushes()
{
	if( !hBRblack ) return;
	DeleteObject( hBRblack );
	DeleteObject( hBackh );
	DeleteObject( hBlackPen );
	DeleteObject( hRedPen );			//l_m: volume
	DeleteObject( hSndOn );
	DeleteObject( hSndOff );
	DeleteObject( hBackDC );
	hBRblack = NULL;
}

//i did not invent this, look at codeguru, "Transparent Bitmap - True Mask Method"
HANDLE CreateMask( HANDLE hSrc, COLORREF colorkey, HDC hSrcDC )
{
	BITMAP bm;
	GetObject( hSrc, sizeof(BITMAP), &bm );
	HBITMAP hMask = CreateBitmap( bm.bmWidth, bm.bmHeight, 1, 1, NULL );	//new bitmap, monochrome
	HDC hTmp = CreateCompatibleDC( hSrcDC );			//mem dc
	SelectObject( hTmp, hSrc );
	SetBkColor( hTmp, colorkey );
	HDC hTmp2 = CreateCompatibleDC( hSrcDC );			//mem dc
	SelectObject( hTmp2, hMask );
	BitBlt( hTmp2, 0, 0, bm.bmWidth, bm.bmHeight, hTmp, 0, 0, SRCCOPY );
	BitBlt( hTmp, 0, 0, bm.bmWidth, bm.bmHeight, hTmp2, 0, 0, SRCINVERT );
	DeleteDC( hTmp );
	DeleteDC( hTmp2 );
	return hMask;
}


//too lazy to make new headers
DWORD __stdcall waitFunc(LPVOID);
void initFMod();
void playFMod();
void stopFMod();
void endFMod();
void setVolumeFMod( DWORD num );		//l_m: volume
DWORD getVolumeFMod();					//l_m: volume


HANDLE hTh;
DWORD tID;

bool bMusic = true;				//play music?
POINT close1[2], close2[2];		//drawing the close icon
POINT volume[4];
POINT volume_slider[2];
bool draggingVolume = false;

INT_PTR CALLBACK dlgProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	HDC hdc, hWindc;
	HBITMAP hBit;

	switch (message) 
	{
	case WM_INITDIALOG:
		hdc = GetDC( hWnd );
		{	//yarr hax to shut the compiler up about case skipped iLeft iTop init blablabla
			RECT rc;
			SystemParametersInfo( SPI_GETWORKAREA, 0, &rc, 0 );
			int iLeft = (rc.right-rc.left)/2 - hBackbmp.bmWidth/2 + rc.left;
			int iTop = (rc.bottom-rc.top)/2 - (hBackbmp.bmHeight + 40)/2 + rc.top;
			SetWindowPos( hWnd, NULL, iLeft, iTop, hBackbmp.bmWidth, hBackbmp.bmHeight + 40, SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_SHOWWINDOW );
		}
		scrollLength = 0;
		for( int n = 0; n < sizeof(scrollTxt)-1; n++ ) {
			GetTextExtentPoint32A( hdc, scrollTxt+n, 1, &scrollSize[n] );
			scrollLength += scrollSize[n].cx;
		}
		GetClientRect( hWnd, &scrollDLG );					//get the inside rectangle, use this when u have borders and want to know size inbetween :)
		close1[0].x = close2[0].x = scrollDLG.right - 12;
		close1[1].x = close2[1].x = scrollDLG.right - 4;
		close1[0].y = close2[1].y = 4;
		close1[1].y = close2[0].y = 12;

		volume[0].x = volume[2].x = volume[3].x = 3;
		volume[1].x = 10;
		volume[0].y = volume[1].y = volume[3].y = 18;
		volume[2].y = 18 + 20;
		
		volume_slider[0].x = 2;
		volume_slider[1].x = 10;

		scrollDLG.top = hBackbmp.bmHeight;
		curPos = scrollDLG.right;
		ReleaseDC( hWnd, hdc );				//what has been aquired, should be released
		SetTimer( hWnd, 1987, 15, NULL );	//redraw timer, 15 ms
		hTh = CreateThread( NULL, NULL, waitFunc, NULL, NULL, &tID );	//wait for game exe, dont hold up drawin :)
		break;
	case WM_TIMER:
		if( wParam == 1987 ) {
			hWindc = GetDC( hWnd );
			hdc = CreateCompatibleDC( hWindc );		//create a memory dc, for creating whole image (flickerfree drawing)
			hBit = CreateCompatibleBitmap( hWindc, scrollDLG.right, scrollDLG.bottom );
			SelectObject( hdc, hBit );			//cant draw on mem buffer without it having a bitmap selected
			SelectObject( hdc, hBRblack );
			if( !hBackDC ) {				//hBackDC is used for selecting a bitmap, before it can be drawn onto hdc
				hBackDC = CreateCompatibleDC( hdc );
				hSndOffMask = CreateMask( hSndOff, RGB(0,255,0), hBackDC );
				hSndOnMask = CreateMask( hSndOn, RGB(0,255,0), hBackDC );
			}
			SelectObject( hBackDC, hBackh );
			BitBlt( hdc, 0, 0, hBackbmp.bmWidth, hBackbmp.bmHeight, hBackDC, 0, 0, SRCCOPY );	//draw the background image
			
			SelectObject( hBackDC, bMusic ? hSndOffMask : hSndOnMask );	//draw snd on/off image + mask
			BitBlt( hdc, 0, 0, 16, 16, hBackDC, 0, 0, SRCAND );
			SelectObject( hBackDC, bMusic ? hSndOff : hSndOn );
			BitBlt( hdc, 0, 0, 16, 16, hBackDC, 0, 0, SRCPAINT );
			
			SelectObject( hdc, hBlackPen );
			Polyline( hdc, close1, 2 );				//close x
			Polyline( hdc, close2, 2 );
			
			Polyline( hdc, volume, 4 );				//l_m: volume
			SelectObject( hdc, hRedPen );
			volume_slider[0].y = volume_slider[1].y = 18 + (20-(getVolumeFMod() / 5));
			
			Polyline( hdc, volume_slider, 2 );

			FillRect( hdc, &scrollDLG, NULL );
			SetBkMode( hdc, TRANSPARENT );			//no filling behind letters
			curPos--;

			if( curPos < -scrollLength ) curPos += scrollLength;
			int lPos = curPos;
			int iHeight = scrollDLG.bottom - scrollDLG.top;
			for( int nxz = 0; nxz < 2; nxz++ )
			{
				for( int n = 0; n < sizeof(scrollTxt)-1; n++ ) {
					if( lPos + scrollSize[n].cx < -2 ) {
					} else if( lPos > scrollDLG.right ) {
						break;
					} else {
						int fy = (int)((sinf( (float)(lPos + nScroll) / (float)scrollDLG.right * (float)M_PI ) * ((float)iHeight/2.0f - (float)scrollSize[n].cy/2.0f) ) + (float)iHeight/4.0f + (float)scrollDLG.top);
						int iCol = (int)((float)(scrollDLG.right - scrollDLG.left) / 255.0f * (float)lPos);
						if( iCol < 0 ) iCol = 0;
						SetTextColor( hdc, RGB(iCol,255,0) );		//color based on current position :)
						ExtTextOutA( hdc, lPos, fy, NULL, &scrollDLG, scrollTxt+n, 1, NULL );
					}
					lPos += scrollSize[n].cx;		//array with width of letters (courier = monospaced, but other fonts aint ;)
				}
			}
			nScroll--;
			BitBlt( hWindc, 0, 0, scrollDLG.right, scrollDLG.bottom, hdc, 0, 0, SRCCOPY );		//draw the constructed dialog onto the real one :)
			DeleteObject( hBit );			//cleaning up never hurts
			DeleteDC( hdc );
			ReleaseDC( hWnd, hWindc );
		}
		break;
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		if( HIWORD(lParam) > 16 || (LOWORD(lParam) > 16 && LOWORD(lParam) < scrollDLG.right -16) ) {
			if( LOWORD(lParam) <= 10 && HIWORD(lParam) <= 38 && HIWORD(lParam) >= 18 ) {	//l_m: volume
				draggingVolume = true;
			} else {
				SendMessage(hWnd, WM_NCLBUTTONDOWN, HTCAPTION,NULL);		//dragging if it aint the sound or close btn
				return TRUE;
			}
			
		}
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		draggingVolume = false;
		if( HIWORD(lParam) <= 16 ) {
			if( LOWORD(lParam) <= 16 ) {
				bMusic = !bMusic;			//pressed music on/off
				if( bMusic )
					playFMod();
				else
					stopFMod();
			} else if( LOWORD(lParam) >= scrollDLG.right -16 ) {
				ExitProcess( 0 );			//pressed close
			}
		}
		if( LOWORD(lParam) <= 10 && HIWORD(lParam) <= 38 && HIWORD(lParam) >= 18 ) {	//l_m: volume
			setVolumeFMod( 100 - (HIWORD(lParam)-18)*5 );
		}
		break;
	case WM_MOUSEMOVE:	//l_m: volume
		if( draggingVolume ) {
			if( LOWORD(lParam) <= 10 && HIWORD(lParam) <= 38 && HIWORD(lParam) >= 18 ) {
				setVolumeFMod( 100 - (HIWORD(lParam)-18)*5 );
			}
		}
		break;
	case WM_KEYUP:
		if( wParam != VK_ESCAPE )
			break;		//if its not escape, break :)
		//else, fall trough \o/
	case WM_DESTROY:
	case WM_CLOSE:
		destroyBrushes();
		PostQuitMessage(0);
		break;
	}
	return 0;
}



int __stdcall WinMain( HINSTANCE, HINSTANCE, LPSTR, int )
{
	initBrushes();
	initFMod();

	HGLOBAL hgbl;
	LPDLGTEMPLATEA lpdt;
	LPWORD lpw;
	LPSTR lpwsz;
	hgbl = GlobalAlloc(GMEM_ZEROINIT, 256);
	lpdt = (LPDLGTEMPLATE)GlobalLock(hgbl);
	lpdt->style = DS_SETFONT | DS_SETFOREGROUND | DS_CENTER | WS_POPUP;		//dont want border, but wanna set font!
	lpdt->dwExtendedStyle = WS_EX_TOPMOST;		//dont be hiding it!
	lpdt->cdit = 0;
	lpdt->x = 0;  
	lpdt->y = 0;

	lpdt->cx = 10; lpdt->cy = 10;	//sadly, this is not in pixels but in custom dialog units :(
									//real calc is done at WM_INITDIALOG with bitmap size
	lpw = (LPWORD) (lpdt + 1);
	*lpw++ = 0;	*lpw++ = 0;			//blah blah

	memcpy( lpw, lpwTitle, (wcslen(lpwTitle)+1)*sizeof(wchar_t) );
	lpw += wcslen((LPWSTR)lpw)+1;

	*lpw++ = (WORD)10;				//fontsize
	lpwsz = (LPSTR) lpw;

	wchar_t fnt[] = L"Courier";		//font...
	memcpy( lpw, fnt, ARRAYSIZE(fnt) );
	lpw += wcslen((LPWSTR)lpw)+1;

	GlobalUnlock( hgbl );
	DialogBoxIndirectParamA( GetModuleHandleA(NULL), (LPDLGTEMPLATE) hgbl, NULL, dlgProc, NULL );	//enters message pump
	GlobalFree( hgbl );

	endFMod();

}


